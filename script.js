var ItemList = function(element) {
    this.array = [];
    this.element = element;

    this.add = function(title) {
        list.append('<div id="' + title + '"></div>');
        var element = $('#' + title);
        var item = new Item(title, element);
        this.array.push(item);
    };

    this.render = function() {
        for (var i = 0; i < this.array.length; ++i) {
            this.array[i].render();
        }
    };
    
    this.remove = function() {
        for (var i = this.array.length - 1; i >= 0; --i) {
            if(this.array[i].marked == true) {
                list.children().eq(i).remove();
                this.array.splice(i, 1);
            }
        }
    }
};

$("#removeButton").on("click", function() {
    itemList.remove();
});

var inputField = $('#inputField');
var list = $('#list');
var itemList = new ItemList(list);

$("#addButton").on("click", function() {
    var title = inputField.val();
    if (title != "") {
        itemList.add(title);
        inputField.val("");
        itemList.render();
    }

});

var Item = function(title, element) {
    this.title = title;
    this.element = element;
    this.marked = false;

    this.render = function() {
        element.text(title);
        if (this.marked == true) {
            this.element.css("background-color", "grey");
        } else {
            this.element.css("background-color", "white");
        }
    };

    this.onClick = function() {
        if(this.marked == false) {
            this.marked = true;
        }
        else {
            this.marked = false;
        }        
        this.render();
    };
    this.element.on("click", this.onClick.bind(this));

};